import React, { Component } from "react";
import todosList from "./todos.json";
import {v4 as uuidv4} from 'uuid';


class App extends Component {
  state = {
    todos: todosList,
  };
  handleToggleButton = (checkId) => {
    console.log("it's working")
    const newTodos = this.state.todos.map((todoItem) => {
      if (todoItem.id === checkId) {
        todoItem.completed = !todoItem.completed;
      }
      return todoItem;
    });
    this.setState({ todos: newTodos });
  };



  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.id !== todoId
    );
    this.setState({ todos: newTodos });
  };


  handleDeleteComplete = () => {
    const newTodos = this.state.todos.filter(
      (todoItem) => todoItem.completed === false);
      this.setState({ todos: newTodos});
  };
    handleAddTodo = (event) => {
      console.log(event)
      const newTodo = {
        "userId": 1,
        "id": uuidv4(),
        "title": this.state.value,
        "completed": false
      }
          console.log(this.state.value)
        const newTodos = [...this.state.todos, newTodo]
        this.setState({todos: newTodos})
        this.setState({value:""})
    }

    
    handleSubmit = (event) => {
      // if(event.key === "Enter"){
        event.preventDefault()
        this.handleAddTodo()
      // }
    
    }
  
    handleChange = (event) => {
      this.setState({value: event.target.value});
     }

  

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit= {this.handleSubmit} >
          <input
            className="new-todo"
            type="text" 
            onChange={this.handleChange}
            value={this.state.value || ''}
            placeholder="What needs to be done?"
            autoFocus
          />
          </form>
        </header>
        <TodoList
          handleDelete={this.handleDelete}
          handleToggleButton={this.handleToggleButton}
          todos={this.state.todos}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteComplete}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              todoId = {todo.id}
              handleToggleButton={() => this.props.handleToggleButton(todo.id)}
              handleDelete={() => this.props.handleDelete(todo.id)}
              title={todo.title}
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onChange={this.props.handleToggleButton}
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={this.props.handleDelete}
          ></button>
        </div>
      </li>
    );
  }
}

  

export default App;
